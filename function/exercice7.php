﻿<?php

function age_genre($age, $genre) {
    $accord = $genre === "femme" ? "e" : "";
    $maj_ou_min = $age < 18 ? "mineur" : "majeur";
    return "Vous êtes un$accord $genre et vous êtes $maj_ou_min$accord";
}

echo age_genre(9, "homme")."\n";
echo age_genre(18, "femme")."\n";

?>
