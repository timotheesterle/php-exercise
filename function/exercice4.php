﻿<?php

function compare_numbers($number1, $number2) {
    if ($number1 > $number2) return "Le premier nombre est plus grand";
    if ($number1 < $number2) return "Le premier nombre est plus petit";
    return "Les deux nombres sont égaux";
}

echo compare_numbers(1, 2)."\n";
echo compare_numbers(2, 1)."\n";
echo compare_numbers(1, 1)."\n";

?>
