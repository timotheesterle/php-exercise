﻿<?php

function sum($num1 = 1, $num2 = 1, $num3 = 1) {
    $args = [$num1, $num2, $num3];
    return array_sum($args);
}

echo "Valeurs par défaut : ".sum()."\n";
echo "Valeurs (3, 4, 8): ".sum(3, 4, 8)."\n";

?>
