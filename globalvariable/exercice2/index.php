<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <title>Superglobales</title>
    </head>
    <body>
        <?php
        session_start();

        $_SESSION["nom"] = "Smith";
        $_SESSION["prenom"] = "John";
        $_SESSION["age"] = 900;
        ?>

        <p>
            <h1>Contenu des variables :</h1>
            <ul>
                <li>
                    Nom :
                    <?= $_SESSION["nom"] ?>
                </li>
                <li>
                    Prénom :
                    <?= $_SESSION["prenom"] ?>
                </li>
                <li>
                    Âge :
                    <?= $_SESSION["age"] ?>
                </li>
            </ul>
        </p>
        <a href="./page2.php">Deuxième page</a>

    </body>
</html>
