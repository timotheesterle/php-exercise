<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <title>Superglobales</title>
    </head>
    <body>
    <?php 
    session_start();
    ?>
        <p>
            <h1>Contenu des variables :</h1>
            <ul>
                <li>
                    Nom :
                    <?= $_SESSION["nom"] ?>
                </li>
                <li>
                    Prénom :
                    <?= $_SESSION["prenom"] ?>
                </li>
                <li>
                    Âge :
                    <?= $_SESSION["age"] ?>
                </li>
            </ul>
        </p>

    </body>
</html>
