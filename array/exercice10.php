﻿<?php

$dpts = [
    02 => "Aisne",
    59 => "Nord",
    60 => "Oise",
    62 => "Pas-de-Calais",
    80 => "Somme"
];

foreach ($dpts as $num => $dpt) {
    echo "Le département $dpt a le numéro $num\n";
}

?>
