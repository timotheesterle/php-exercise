<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <title>Formulaire</title>
    </head>
    <body>
        <?php if(!$_POST): ?>

        <form action="index.php" method="POST">
            <label for="nom">Civilité :</label>
            <select id="civilite" name="civilite">
                <option value="Mme.">Mme.</option>
                <option value="M.">M.</option>
                <option value="Mx.">Mx.</option>
            </select>
            <label for="nom">Nom :</label>
            <input id="nom" type="text" name="nom" />
            <label for="prenom">Prénom :</label>
            <input id="prenom" type="text" name="prenom" />
            <input id="fichier" type="file" name="fichier" />
            <button type="submit">Envoyer</button>
        </form>
    </body>

    <?php else: ?>

    <p>
        <?= "Bonjour {$_POST["civilite"]} {$_POST["prenom"]} {$_POST["nom"]} !" ?>
    </p>

    <p>
        <?= "Vous venez d'envoyer le fichier <em>\"{$_POST["fichier"]}\"</em>"?>
    </p>

    <?php endif; ?>
</html>
