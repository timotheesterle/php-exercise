<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <title>Formulaire</title>
    </head>
    <body>
        <form action="index.php" method="POST">
            <label for="nom">Civilité :</label>
            <select id="civilite" name="civilite">
                <option value="mme">Mme.</option>
                <option value="m">M.</option>
                <option value="mx">Mx.</option>
            </select>
            <label for="nom">Nom :</label>
            <input id="nom" type="text" name="nom" />
            <label for="prenom">Prénom :</label>
            <input id="prenom" type="text" name="prenom" />
            <button type="submit">Envoyer</button>
        </form>
    </body>
</html>
